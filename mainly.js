(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.sbn = factory());
}(this, (function () { 'use strict';


function lexer (code) {
  return code.split(/\s+/)
    .filter(function (t) { return t.length > 0 })
    .map(function (t) {
      return isNaN(t)
              ? {type: 'word', value: t}
              : {type: 'number', value: t}
    })
}

function parser (tokens) {
  var AST = {
    type: 'Drawing',
    body: []
  }

  while (tokens.length > 0){
    var current_token = tokens.shift()

    if (current_token.type === 'word') {
      switch (current_token.value) {
        case 'Papel' :
          var expression = {
            type: 'CallExpression',
            name: 'Paper',
            arguments: []
          }

          var argument = tokens.shift()
          if(argument.type === 'number') {

            expression.arguments.push({
              type: 'NumberLiteral',
              value: argument.value
            })

            AST.body.push(expression)
          } else {
            throw 'Papel debe ir seguido de un número.'
          }
          break

        case 'Lapiz' :
          var expression = {
            type: 'CallExpression',
            name: 'Pen',
            arguments: []
          }

          var argument = tokens.shift()
          if(argument.type === 'number') {
            expression.arguments.push({
              type: 'NumberLiteral',
              value: argument.value
            })

            AST.body.push(expression)
          } else {
            throw 'Lapiz debe ir seguido de un número..'
          }
          break

        case 'Linea' :
          var expression = {
            type: 'CallExpression',
            name: 'Line',
            arguments: []
          }

          for (var i = 0; i < 4; i++) {
            var argument = tokens.shift()
            if(argument.type === 'number') {

              expression.arguments.push({
                type: 'NumberLiteral',
                value: argument.value
              })
            } else {
              throw 'Linea debe ir seguido de 4 números.'
            }
          } 
          AST.body.push(expression)
          break

          case 'Circulo' :
          var expression = {
            type: 'CallExpression',
            name: 'Circle',
            arguments: []
          }

          for (var i = 0; i < 3; i++) {
            var argument = tokens.shift()
            if(argument.type === 'number') {

              expression.arguments.push({
                type: 'NumberLiteral',
                value: argument.value
              })
            } else {
              throw 'Circulo debe ir seguido de 3 números.'
            }
          } 
          AST.body.push(expression)
          break

          case 'Imprimir' :
          var expression = {
            type: 'CallExpression',
            name: 'Text',
            arguments: []
          }

          for (var i = 0; i < 2; i++) {
            var argument = tokens.shift()
            if(argument.type === 'word') {

              expression.arguments.push({
                type: 'WordAttribute',
                value: argument.value
              })
            } else {
              throw 'Imprimir debe ir seguido de un Texto y Color.'
            }
          } 

          for (var i = 0; i < 3; i++) {
            var argument = tokens.shift()
            if(argument.type === 'number') {

              expression.arguments.push({
                type: 'NumberLiteral',
                value: argument.value
              })
            } else {
              throw 'Imprimir debe ir seguido de Palabra , Color y 3 números.'
            }
          } 
          AST.body.push(expression)
          break

          case 'MultiLinea' :
          var expression = {
            type: 'CallExpression',
            name: 'Polyline',
            arguments: []
          }

          for (var i = 0; i < 2; i++) {
            var argument = tokens.shift()
            if(argument.type === 'word') {

              expression.arguments.push({
                type: 'WordAttribute',
                value: argument.value
              })
            } else {
              throw 'MultiLinea debe ir seguido de un Color y Relleno.'
            }
          } 

          for (var i = 0; i < 8; i++) {
            var argument = tokens.shift()
            if(argument.type === 'number') {

              expression.arguments.push({
                type: 'NumberLiteral',
                value: argument.value
              })
            } else {
              throw 'MultiLinea debe ir seguido de  Color, Relleno y 8 números.'
            }
          } 
          AST.body.push(expression)
          break
      }
    }
  }
  return AST
}


function transformer (ast) {

  var svg_ast = {
    tag : 'svg',
    attr: {
      width: 100,
      height: 100,
      viewBox: '0 0 100 100',
      xmlns: 'http://www.w3.org/2000/svg',
      version: '1.1'
    },
    body:[]
  }

  var pen_color = 100

  while (ast.body.length > 0) {
    var node = ast.body.shift()
    switch (node.name) {
      case 'Paper' :
        var paper_color = 100 - node.arguments[0].value
        svg_ast.body.push({
          tag : 'rect',
          attr : {
            x: 0,
            y: 0,
            width: 100,
            height:100,
            fill: 'rgb(' + paper_color + '%,' + paper_color + '%,' + paper_color + '%)'
          }
        })
        break
      case 'Pen':
        pen_color = 100 - node.arguments[0].value
        break
      case 'Line':
        svg_ast.body.push({
          tag: 'line',
          attr: {
            x1: node.arguments[0].value,
            y1: node.arguments[1].value,
            x2: node.arguments[2].value,
            y2: node.arguments[3].value,
            'stroke-linecap': 'round',
            stroke: 'rgb(' + pen_color + '%,' + pen_color + '%,' + pen_color + '%)'
          }
        })
        break

      case 'Circle':
        svg_ast.body.push({
          tag: 'circle',
          attr: {
            cx: node.arguments[0].value,
            cy: node.arguments[1].value,
            r: node.arguments[2].value, 
            'stroke-linecap': 'round',
            stroke: "white",
            fill: "black",
          }
        })
        break

        case 'Text':
        svg_ast.body.push({
          tag: 'text',
          attr: {
            fill: node.arguments[1].value,
           'font-size': node.arguments[2].value, 
            x: node.arguments[3].value,
            y: node.arguments[4].value,
            'font-family': 'Verdana'
          },
          text: node.arguments[0].value
        })
        break

        case 'Polyline':
        svg_ast.body.push({
          tag: 'polyline',
          attr: {
            points: node.arguments[2].value+','+node.arguments[3].value+','+node.arguments[4].value+','+node.arguments[5].value+','+
                    node.arguments[6].value+','+node.arguments[7].value+','+node.arguments[8].value+','+node.arguments[9].value,
            'style': 'fill:'+  node.arguments[1].value +';stroke:'+  node.arguments[0].value+';stroke-width:1',
           
          }
        })
        break
    }
  }

  return svg_ast
}

function generator (svg_ast) {

  function createAttrString (attr) {
    return Object.keys(attr).map(function (key){
      return key + '="' + attr[key] + '"'
    }).join(' ')
  }

  var svg_attr = createAttrString(svg_ast.attr)

  var elements = svg_ast.body.map(function (node) {
    var a = node.text? node.text: '';
    console.log('Try '+ a);
    return '<' + node.tag + ' ' + createAttrString(node.attr) + '>'+ a +'</' + node.tag + '>'
  }).join('\n\t')

  return '<svg '+ svg_attr +'>\n' + elements + '\n</svg>'
}

var sbn = {}
sbn.VERSION = '0.0.1'
sbn.lexer = lexer
sbn.parser = parser
sbn.transformer = transformer
sbn.generator = generator
sbn.compile = function (code) {
  return this.generator(this.transformer(this.parser(this.lexer(code))))
}

return sbn;

})));

/* Inicial Binevenida UPSLP 

Papel 100
Lapiz 0
Linea 50 15 85 80
Linea 85 80 15 80
Linea 15 80 50 15
Imprimir Compiladores white 10 15 50
Circulo 50 65 10
Imprimir UPSLP white 5 42 66
*/
