
export function lexer (code) {
  return code.split(/\s+/)
          .filter(function (t) { return t.length > 0 })
          .map(function (t) {
            return isNaN(t)
                    ? {type: 'word', value: t}
                    : {type: 'number', value: t}
          })
}

export function parser (tokens) {
  var AST = {
    type: 'Drawing',
    body: []
  }

  while (tokens.length > 0){
    var current_token = tokens.shift()

      switch (current_token.value) {
        case 'Papel' :
          var expression = {
            type: 'CallExpression',
            name: 'Paper',
            arguments: []
          }

          var argument = tokens.shift()
          if(argument.type === 'number') {

            expression.arguments.push({
              type: 'NumberLiteral',
              value: argument.value
            })

            AST.body.push(expression)
          } else {
            throw 'Papel debe ir seguido de un número.'
          }
          break

        case 'Lapiz' :
          var expression = {
            type: 'CallExpression',
            name: 'Pen',
            arguments: []
          }

          var argument = tokens.shift()
          if(argument.type === 'number') {
            expression.arguments.push({
              type: 'NumberLiteral',
              value: argument.value
            })
            AST.body.push(expression)
          } else {
            throw 'Lapiz debe ir seguido de un número..'
          }
          break

        case 'Linea':
          var expression = {
            type: 'CallExpression',
            name: 'Line',
            arguments: []
          }

          for (var i = 0; i < 4; i++) {
            var argument = tokens.shift()
            if(argument.type === 'number') {
              expression.arguments.push({
                type: 'NumberLiteral',
                value: argument.value
              })
            } else {
              throw 'Linea debe ir seguido de 4 números.'
            }
          }
          AST.body.push(expression)
          break
      }
    }
  }
  return AST

